cmake_minimum_required(VERSION 3.16)
project(sdpt_2022)

set(CMAKE_CXX_STANDARD 17)
include_directories(src src/printerLibrary)

# set(SOURCE_FILES src/main.cpp src/printerLibrary/printer.cpp src/printerLibrary/printer.hpp)

file(GLOB_RECURSE SOURCE_FILES src/*.cpp src/*.hpp)
add_executable(main ${SOURCE_FILES})