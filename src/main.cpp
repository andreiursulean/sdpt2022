#include <iostream>
#include <cstdint>
#include <IntegerToRomanNumeralConverter.hpp>

int main(int32_t argc, char** args) {
    try {
        if (argc < 2 ) {
            std::cerr << "No input value given!" << std::endl;
            return 0;
        }

        uint32_t inputValue = std::stoi(args[1]);
        std::cout << "Converted number is: " << IntegerToRomanNumeralConverter().convert(inputValue) << std::endl;
    } catch(...) {
        std::cerr << "Invalid input value to the roman numeral converter: " << args[1] << std::endl;
    }

    return 0;
}