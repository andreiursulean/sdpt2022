#include "IntegerToRomanNumeralConverter.hpp"

std::string IntegerToRomanNumeralConverter::convert(uint32_t value) {
    if (value < 4) {
        return std::string(value, 'I');
    }

    return value == 4 ? "IV" : "VI";
}