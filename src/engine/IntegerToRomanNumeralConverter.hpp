#pragma once

#include <string>
#include <cstdint>

struct IntegerToRomanNumeralConverter {
    std::string convert(uint32_t value);
    IntegerToRomanNumeralConverter()  = default;
};