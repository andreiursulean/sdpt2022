SUBDIRS = $(shell find src/ -type d)
PROFILE = Release

BASE_FLAGS = -c --std=c++17 -MMD -I. $(addprefix -I, $(SUBDIRS)) -I/usr/include/library1

ifeq ($(PROFILE), Release)
CXXFLAGS = $(BASE_FLAGS) -O3 
else ifeq ($(PROFILE), Debug)
CXXFLAGS = $(BASE_FLAGS) -Og -g
endif

CPP_FILES = $(shell find src/ -name "*.cpp")
OBJ_FILES = $(CPP_FILES:%.cpp=$(PROFILE)/%.o)
D_FILES = $(CPP_FILES:%.cpp=$(PROFILE)/%.d)

TEST_CPP_FILES = $(shell find tests/ -name "*.cpp")
TEST_OBJ_FILES = $(TEST_CPP_FILES:%.cpp=$(PROFILE)/%.o)
TEST_D_FILES = $(TEST_CPP_FILES:%.cpp=$(PROFILE)/%.d)

LDFLAGS = -lpthread
EXE_NAME = main
UTEST_NAME = utest

.PHONY: all
all: $(PROFILE)/$(EXE_NAME) $(PROFILE)/$(UTEST_NAME)

#this is using static library - .a
$(PROFILE)/$(EXE_NAME): $(OBJ_FILES)
	$(CXX) $^ -o $@ $(LDFLAGS)

$(PROFILE)/src/%.o: src/%.cpp
	mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) $< -o $@

$(PROFILE)/tests/%.o: tests/%.cpp
	mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) $< -o $@	

$(PROFILE)/$(UTEST_NAME): $(filter-out $(PROFILE)/src/main.o, $(OBJ_FILES)) $(TEST_OBJ_FILES)
	$(CXX) $^ -o $@ -lgtest_main -lgtest $(LDFLAGS)  
-include $(D_FILES)

.PHONY: clean distclean
clean: 
	rm -rf $(PROFILE)

distclean:
	rm -rf Debug Release 