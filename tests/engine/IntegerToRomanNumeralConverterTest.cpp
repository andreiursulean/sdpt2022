#include <gtest/gtest.h>
#include <src/engine/IntegerToRomanNumeralConverter.hpp>

class IntegerToRomanNumeralConverterTestFixture : public ::testing::Test {
protected:
    IntegerToRomanNumeralConverter converter;

    void SetUp() override {
        std::cout << "This is the setup!" << std::endl;
    }

    void TearDown() override {
        std::cout << "This is the teardown!" << std::endl;
    }
};

TEST_F(IntegerToRomanNumeralConverterTestFixture, TestIf1ReturnsI) {
    ASSERT_EQ(converter.convert(1), "I");
}

TEST_F(IntegerToRomanNumeralConverterTestFixture, TestIf2ReturnsII) {
    ASSERT_EQ(converter.convert(2), "II");
}

TEST_F(IntegerToRomanNumeralConverterTestFixture, TestIf4ReturnsIV) {
    ASSERT_EQ(converter.convert(4), "IV");
}

TEST_F(IntegerToRomanNumeralConverterTestFixture, TestIf6ReturnsVI) {
    EXPECT_EQ(converter.convert(6), "VI");
}